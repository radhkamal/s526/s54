let collection = [];


// Write the queue functions below.


// 1. Output all the elements of the queue

		function print() {
			return collection;
		}


// 2. Adds element to the rear of the queue

		function enqueue(e) {

			collection[collection.length] = e;

			return collection;
		}


// 3. Removes element from the front of the queue

		function dequeue() {

			let newCollection = [];
			let index = 0;

			for (i = 1; i < collection.length; i++) {
				newCollection[index] = collection[i];
				index++;
			}

			collection = [...newCollection];
			return collection;
		}



// 4. Show element at the front

		function front() {
			return collection[0];
		}



// 5. Show the total number of elements

		function size() {
			return collection.length;
		}



// 6. Outputs a Boolean value describing whether queue is empty or not


		function isEmpty() {
			if (collection.length > 0) {
				return false;
			} else {
				return true;
			}
		}

module.exports = {
	print,
	enqueue,
	dequeue,
	front, 
	total,
	isEmpty
};